<?php

use Illuminate\Support\Facades\Route;
use app\Http\Livewire\StudentData;
// Route::get('/main', function () {
//     return view('layout.main');
// });

Route::get('/', function () {
    return view('layout.login');
})->name('login');

Route::get('/register', function () {
    return view('layout.register');
});

Route::get('/sidebar', function () {
    return view('layout.sidebar');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', function () {
        return view('layout.dashboard');
    });
    Route::get('/main', function () {
        return view('layout.main');
    });
    Route::get('/payment_history', function () {
        return view('layout.payment_history');
    });
    
});
